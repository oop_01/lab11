package com.manita.week11;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Bird bird1 = new Bird("Tweety");
        bird1.sleep();
        bird1.eat();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();
        Plane boeing = new Plane("Boeing", "Rosaroi");
        boeing.takeoff();
        boeing.fly();
        boeing.landing();
        Superman clark = new Superman("Clark");
        clark.eat();
        clark.sleep();
        clark.takeoff();
        clark.fly();
        clark.landing();
        clark.walk();
        clark.run();
        clark.swim();
        Human man1 = new Human("Man");
        man1.swim();
        man1.eat();
        man1.sleep();
        man1.walk();
        man1.run();
        Fish fish1 = new Fish("Tuna");
        fish1.swim();
        Crocodile crocodile = new Crocodile("Gigi");
        crocodile.crawl();
        Dog dog = new Dog("Koko");
        dog.eat();
        dog.sleep();
        dog.run();
        Cat black = new Cat("Black");
        black.eat();
        black.walk();
        black.swim();
        Bat bat1 = new Bat("Dollar");
        bat1.eat();
        Snake snake1 = new Snake("Joey");
        snake1.crawl();
        Rat rat1 = new Rat("Max");
        rat1.eat();
        rat1.swim();
        Bus bus1 = new Bus("Bus", "mercedes-benz");
        Submarine submarine1 = new Submarine("Too", "Drebbel");
        submarine1.swim();

        Flyable[] flyables = { bird1, boeing, clark, bat1 };
        for (int i = 0; i < flyables.length; i++) {
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }
        Walkable[] walkable = { bird1, man1, clark, dog, black, rat1, crocodile };
        for (int i = 0; i < walkable.length; i++) {
            walkable[i].walk();
            walkable[i].run();
        }
        Crawlable[] crawlable = { crocodile, snake1 };
        for (int i = 0; i < crawlable.length; i++) {
            crawlable[i].crawl();
        }
        Swimable[] swimable = { crocodile, fish1, dog, black, rat1, man1, clark };
        for (int i = 0; i < swimable.length; i++) {
            swimable[i].swim();
        }
    }
}
